Console for PHP applications development
========================================

This image provides console functionality for PHP applications, that requires 
interpreter version 5.4.

Installed components
--------------------

* php 5.4 (cli)
* xcache
* composer

Usage
-----

Running `composer update` command:

```
docker run -it --rm \
        -u $UID \
        -w /src -v $PWD:/src \
        -v $HOME/.composer:/.composer \
    minity/php54-console composer update
```


