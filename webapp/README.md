PHP web-application server
=======================================

This image provides web-application server functionality for apps
required PHP interpreter version 5.4.

Installed components
--------------------

* php 5.4 (fpm)
* nginx-light
* nullmailer SMTP-server

Configuration
-------------

Web server's document root points to `/src/www` directory.

All requests, which aren't resolved as files, are forwarded
to front controller `/src/www/index.php`.

Environment variable | Descrition                                                                                                    | Default value
---------------------|---------------------------------------------------------------------------------------------------------------|-------------------------
`VIRTUAL_HOST`       | Used as host name in mail sender address. Also can be used with https://hub.docker.com/r/jwilder/nginx-proxy/ | (required)
`SMTP_ADDRESS`       | Mail Server address. If not set then application can not send mails.                                          | (undefined)
`SMTP_PORT`          | Mail Server TCP port                                                                                          | 25
`SMTP_USER`          | Mail Server user name                                                                                         | (no user required)
`SMTP_PASSWORD`      | Mail Server user password                                                                                     | (no password required)
`APP_UID`            | Linux user ID for application running                                                                         | 1000
`APP_GID`            | Linux user group ID for application running                                                                   | 1000

Usage
-----

Running web-application:

```
docker run -d --name myapp \
        -e VIRTUAL_HOST=myapp.local \
        -e SMTP_ADDRESS=smtp.example.com \
        -e SMTP_PORT=2025 \
        -e SMTP_USER=smtp_user \
        -e SMTP_PASSWORD=smtp_secret \
        -e APP_UID=1001 \
        -v $PWD:/src \
    minity/php54-webapp
```


