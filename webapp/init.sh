#!/bin/bash

groupmod -n user "${APP_GID:-${APP_UID:-1000}}"
usermod -l user "${APP_UID:-1000}"

echo "info@${VIRTUAL_HOST}" > /etc/nullmailer/adminaddr
echo "${SMTP_ADDRESS} smtp${SMTP_PORT:+ --port=$SMTP_PORT}${SMTP_USER:+ --user=$SMTP_USER}${SMTP_PASSWORD:+ --pass=$SMTP_PASSWORD}" > /etc/nullmailer/remotes

for script in /init.d/*; do [ -f "$script" ] && echo -n "executing $script ..." && /bin/bash "$script" && echo " done"; done
