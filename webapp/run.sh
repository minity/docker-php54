#!/bin/bash

[ -f /init.sh ] && /bin/bash /init.sh && rm /init.sh

cron && \
    ( [ -n "$SMTP_ADDRESS" ] && /etc/init.d/nullmailer start || true ) && \
    php5-fpm --daemonize && \
    nginx -g "daemon off;"