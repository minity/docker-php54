Console for PHP applications development
========================================

This image provides console functionality for PHP applications, that requires.
interpreter version 5.4.

Installed components
--------------------

* php 5.4 (cli)
* xcache
* xdebug
* xhprof
* composer

Usage
-----

Running `composer update` command:

```
docker run -it --rm \
        -u $UID \
        -w /src -v $PWD:/src \
        -v $HOME/.composer:/.composer \
    minity/php54-console composer update
```

Profiling
---------

You can add following code to your project's bootstrap file:

```php
if (isset($_ENV['XHPROF_SESSION']) || isset($_GET['XHPROF_SESSION']) || isset($_COOKIE['XHPROF_SESSION'])) {
    include_once "xhprof_lib/utils/xhprof_lib.php";
    include_once "xhprof_lib/utils/xhprof_runs.php";

    xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);

    register_shutdown_function(function () {
        if (function_exists('fastcgi_finish_request')) fastcgi_finish_request();
        $category = isset($_GET['XHPROF_SESSION'])
            ? $_GET['XHPROF_SESSION']
            : ( isset($_COOKIE['XHPROF_SESSION'])
                ? $_COOKIE['XHPROF_SESSION']
                : $_ENV['XHPROF_SESSION'] 
            );
        (new XHProfRuns_Default())->save_run(xhprof_disable(), $category);
    });
}
```

then profiles will be saved into container's volume `/tmp/xhprofiles`